import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import RefreshIcon from "@mui/icons-material/Refresh";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import AddIcon from "@mui/icons-material/AddCircleOutline";
import AccountIcon from "@mui/icons-material/AccountCircle";
import Logout from "@mui/icons-material/Logout";
import HomeIcon from "@mui/icons-material/HomeRounded";

import Tooltip from "../../../components/Tooltip";
import TrainRequestModal from "../../../components/train-request-modal";
import UserService from "../../../services/UserService";
import {
  getOpenRequestModal,
  setRequestModal,
  fetchJobs,
} from "../trainsSlice";

export default function NavBar() {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openRequestModal = useSelector(getOpenRequestModal);
  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleProfileMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = "account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleProfileMenuClose}
    >
      <MenuItem onClick={() => UserService.doLogout()}>
        <ListItemIcon>
          <Logout color="primary" fontSize="small" />
        </ListItemIcon>
        Logout
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <AppBar elevation={0}>
        <Toolbar sx={{ py: 1 }}>
          <img src="./logo-only.png" style={{ maxWidth: "60px" }} />
          <Typography variant="h6" ml={2} fontWeight="bold">
            PHT CENTER SERVICE
          </Typography>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <Tooltip title="Home">
              <IconButton
                href="https://padme-analytics.de"
                target="_blank"
                size="large"
                color="inherit"
              >
                <HomeIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Refresh">
              <IconButton
                onClick={() => dispatch(fetchJobs())}
                size="large"
                color="inherit"
              >
                <RefreshIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="New Train Request">
              <IconButton
                onClick={() => dispatch(setRequestModal(true))}
                size="large"
                color="inherit"
              >
                <AddIcon />
              </IconButton>
            </Tooltip>
            <Button
              aria-label="user account"
              aria-controls={menuId}
              aria-haspopup="true"
              size="large"
              startIcon={<AccountIcon />}
              onClick={handleProfileMenuOpen}
              sx={{ color: "white" }}
            >
              {UserService.getUsername()}
            </Button>
            {renderMenu}
          </Box>
        </Toolbar>
      </AppBar>
      <Toolbar />
      <TrainRequestModal
        open={openRequestModal}
        handleClose={() => dispatch(setRequestModal(false))}
      />
    </>
  );
}
