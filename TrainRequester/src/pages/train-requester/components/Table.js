import * as React from "react";
import { useSelector, useDispatch } from "react-redux";
import Container from "@mui/material/Container";
import Chip from "@mui/material/Chip";
import Tooltip from "@mui/material/Tooltip";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import Skeleton from "@mui/material/Skeleton";
import DownloadIcon from "@mui/icons-material/CloudDownload";
import VisibilityIcon from "@mui/icons-material/Visibility";
import SwipeLeftIcon from "@mui/icons-material/SwipeLeft";

import TableHead from "./TableHead";
import TableToolbar from "./TableToolbar";
import TablePaginationActions from "./TablePaginationActions";
import { stableSort, getComparator, renderStationID } from "../utils";
import { columnLabel, trainStates } from "../constants";
import {
  fetchJobs,
  fetchTrains,
  fetchStations,
  fetchJobResult,
  selectAllJobs,
  setRejectModal,
  getTableRows,
  getJobsStatus,
  getJobsError,
  getOpenRejectModal,
  resetResultTab,
  isLoading,
} from "../trainsSlice";
import TrainRouteModal from "../../../components/train-route-modal";
import JobResultModal from "../../../components/job-result-modal";
import TrainRejectModal from "../../../components/train-reject-modal";

const RequesterTable = () => {
  const dispatch = useDispatch();

  const jobs = useSelector(selectAllJobs);
  const rows = useSelector(getTableRows);
  const jobsStatus = useSelector(getJobsStatus);
  const error = useSelector(getJobsError);
  const loading = useSelector(isLoading);
  const openRejectModal = useSelector(getOpenRejectModal);

  React.useEffect(() => {
    dispatch(fetchStations());
    dispatch(fetchTrains());
    dispatch(fetchJobs());
  }, [dispatch]);

  const [rowSelected, setRowSelected] = React.useState(jobs[0]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [order, setOrder] = React.useState("desc");
  const [orderBy, setOrderBy] = React.useState(columnLabel.LAST_UPDATE.key);
  const [openRoute, setOpenRoute] = React.useState(false);
  const [openResult, setOpenResult] = React.useState(false);

  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const renderTrainRoute = (row) => {
    const handleRouteDetails = () => {
      setRowSelected(row);
      setOpenRoute(true);
    };

    return (
      <Tooltip title="Show details">
        <IconButton
          size="large"
          aria-label="show details"
          onClick={handleRouteDetails}
        >
          <VisibilityIcon />
        </IconButton>
      </Tooltip>
    );
  };

  const renderCurrentStatus = (row) => {
    const status = row.currentStatus.replace(/_/g, " ");
    let props = {
      sx: { width: 100, fontWeight: "bold" },
      color: "default",
    };

    switch (status) {
      case trainStates.REJECT:
        props.color = "error";
        props.icon = <SwipeLeftIcon />;
        props.onClick = () => {
          setRowSelected(row);
          dispatch(setRejectModal(true));
        };
        break;
      case trainStates.PULLED:
        props.color = "info";
        break;
      case trainStates.FINISHED:
        props.color = "success";
        break;
    }
    return <Chip {...props} label={status} />;
  };

  const LoadSkeletonRows = () => {
    const skeletonArray = Array(rowsPerPage).fill(""); //
    return skeletonArray.map((_, ridx) => (
      <TableRow hover key={`skeleton-row-${ridx}`}>
        {Array(rowsPerPage - 1) // We have 9 Columns. This can be generalized in future.
          .fill("")
          .map((__, cidx) => (
            <TableCell key={`skeleton-col-${cidx}-${ridx}`}>
              <Skeleton variant="text" height={30} />
            </TableCell>
          ))}
      </TableRow>
    ));
  };

  return (
    <Container sx={{ my: 5 }} maxWidth="xl">
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableToolbar rows={jobs} setPage={setPage} />
        <TableContainer sx={{ maxHeight: 900 }}>
          <Table
            stickyHeader
            aria-label="Requested Trains"
            sx={{ minWidth: 650, tableLayout: "auto" }}
          >
            <TableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {loading ? (
                <LoadSkeletonRows />
              ) : (
                stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <TableRow hover key={row.jobID}>
                        <TableCell>{row.id}</TableCell>
                        <TableCell>{row.jobID}</TableCell>
                        <TableCell>{row.trainClass}</TableCell>
                        <TableCell>
                          {renderStationID(row.currentStation)}
                        </TableCell>
                        <TableCell>
                          {renderStationID(row.nextStation)}
                        </TableCell>
                        <TableCell>{renderCurrentStatus(row)}</TableCell>
                        <TableCell>{renderTrainRoute(row)}</TableCell>
                        <TableCell>
                          {new Date(row.lastUpdate).toLocaleString("de-DE")}
                        </TableCell>
                        <TableCell>
                          <Tooltip
                            title={
                              row.currentStatus === trainStates.FINISHED
                                ? "Download"
                                : "Not available"
                            }
                          >
                            <div>
                              <IconButton
                                disabled={
                                  row.currentStatus !== trainStates.FINISHED
                                }
                                size="large"
                                aria-label="show details"
                                onClick={() => {
                                  setRowSelected(row);
                                  dispatch(resetResultTab());
                                  dispatch(fetchJobResult(row.jobID));
                                  setOpenResult(true);
                                }}
                              >
                                <DownloadIcon />
                              </IconButton>
                            </div>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          rowsPerPageOptions={[10, 25, 50, { label: "All", value: -1 }]}
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          sx={{ "& .MuiToolbar-root": { pr: 2 } }}
          SelectProps={{
            inputProps: {
              "aria-label": "rows per page",
            },
            native: true,
          }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </Paper>
      <TrainRouteModal
        open={openRoute}
        handleClose={() => setOpenRoute(false)}
        selected={rowSelected}
      />
      <TrainRejectModal
        open={openRejectModal}
        handleClose={() => dispatch(setRejectModal(false))}
        selected={rowSelected}
      />
      <JobResultModal
        open={openResult}
        handleClose={() => setOpenResult(false)}
        selected={rowSelected}
      />
    </Container>
  );
};

export default RequesterTable;
