import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import axios from "../../services/AxiosService";
import { createData } from "./utils";
import { resultTab, resultItemSupportedTypes } from "./constants";

const imageRegex = /\.(gif|jpe?g|tiff?|png|webp|bmp)$/i;
const textFileRegex = /\.(txt|csv)$/i;

/**
 * Async Thunk functions
 */
export const fetchJobs = createAsyncThunk(
  "trains/fetchJobs",
  async (_, thunkAPI) => {
    try {
      const response = await axios.get(`/api/jobinfo`);
      return [...response.data];
    } catch (error) {
      const errorMessage = `Failed to fetch jobs. ${error.message}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/fetchJobs",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const requestTrain = createAsyncThunk(
  "trains/requestTrain",
  async (payload, thunkAPI) => {
    try {
      const { data } = await axios.post(`/api/jobinfo`, { ...payload });
      thunkAPI.dispatch(
        showAlert({
          message: "The train request has been successfully sent.",
          options: {
            key: "trains/requestTrain",
            variant: "success",
          },
        })
      );

      return createData(
        1,
        data.jobid,
        data.trainclassid,
        data.currentstation,
        data.nextstation,
        data.currentstate,
        data.route,
        data.updatedAt,
        data.stationmessages,
        data.visited
      );
    } catch (error) {
      const errorMessage =
        "Job could not be created, please look at the log files.";

      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/requestTrain",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const rejectTrain = createAsyncThunk(
  "trains/rejectTrain",
  async (payload, thunkAPI) => {
    try {
      const response = await axios.post(`/api/jobinfo/skipCurrentStation`, {
        ...payload,
      });
      thunkAPI.dispatch(
        showAlert({
          message: "The request has been successfully sent.",
          options: {
            key: "trains/rejectTrain",
            variant: "success",
          },
        })
      );
      thunkAPI.dispatch(fetchJobs());
    } catch (error) {
      const errorMessage = `Something went wrong. ${error.message}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/rejectTrain",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const fetchStations = createAsyncThunk(
  "trains/fetchStations",
  async (_, thunkAPI) => {
    try {
      const response = await axios.get(`/api/stations`);
      return [...response.data];
    } catch (error) {
      const errorMessage = `Failed to fetch stations. ${error.message}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/fetchStations",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const fetchJobResult = createAsyncThunk(
  "trains/fetchJobResult",
  async (jobId, thunkAPI) => {
    try {
      const response = await axios.get(`/api/jobresult/${jobId}`);
      return [...response.data];
    } catch (error) {
      const errorMessage = `Job results not available for id: ${jobId}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: jobId,
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const downloadJobResult = createAsyncThunk(
  "trains/downloadJobResult",
  async ({ jobId, file = null }, thunkAPI) => {
    try {
      const downloadLink = `/api/jobresult/${jobId}/download`;
      const response = await axios.get(
        !file ? downloadLink : `${downloadLink}/?path=${file}`,
        { responseType: "blob" }
      );
      // Create file link in browser's memory
      const href = URL.createObjectURL(response.data);
      const contentDisposition = response.headers["content-disposition"];
      const filename = contentDisposition.match(/filename=(.+)/)[1];

      var anchor = document.createElement("a");
      anchor.setAttribute("href", href);
      anchor.setAttribute("download", filename);
      anchor.setAttribute("target", "_blank");
      anchor.click();

      // Release object to prevent memory leak
      URL.revokeObjectURL(href);
    } catch (error) {
      const errorMessage = `Download failed for job id: ${jobId}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/downloadJobResult",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const downloadResultItem = createAsyncThunk(
  "trains/downloadResultItem",
  async ({ jobId, file }, thunkAPI) => {
    try {
      const config = imageRegex.test(file) ? { responseType: "blob" } : {};
      const response = await axios.get(
        `/api/jobresult/${jobId}/download/?path=${file}`,
        config
      );
      return { content: response.data, file };
    } catch (error) {
      const errorMessage = `Failed to view file: ${file}. ${error.message}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/downloadResultItem",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

export const fetchTrains = createAsyncThunk(
  "trains/fetchTrains",
  async (_, thunkAPI) => {
    try {
      const response = await axios.get(`/api/harbor/trains`);
      return [...response.data];
    } catch (error) {
      const errorMessage = `Failed to fetch trains. ${error.message}`;
      thunkAPI.dispatch(
        showAlert({
          message: errorMessage,
          options: {
            key: "trains/fetchTrains",
            variant: "error",
          },
        })
      );

      return thunkAPI.rejectWithValue({ error: errorMessage });
    }
  }
);

const initialState = {
  jobs: [],
  rows: [],
  trains: [],
  stations: [],
  jobResult: [],
  notifications: [],
  downloadFiles: [],
  loading: false,
  status: "idle",
  error: "",
  openTab: 0, // List Tab: 0, View Tab: 1
  openTabId: resultTab.LIST,
  selectedFile: "",
  resultItem: { type: "", content: null },

  openRequestModal: false,
  openRejectModal: false,
};

const trainsSlice = createSlice({
  name: "trains",
  initialState,
  reducers: {
    setTableRows: (state, { payload }) => {
      state.rows = payload;
    },
    setRequestModal: (state, { payload }) => {
      state.openRequestModal = payload;
    },
    setRejectModal: (state, { payload }) => {
      state.openRejectModal = payload;
    },
    setDownloadFiles: (state, { payload }) => {
      state.downloadFiles = payload;
    },
    setOpenTab: (state, { payload }) => {
      state.openTab = payload.tab;
      state.openTabId = payload.tabId;
    },
    setSelectedFile: (state, { payload }) => {
      state.selectedFile = payload;
    },
    resetResultTab: (state) => {
      if (state.resultItem.type === resultItemSupportedTypes.IMAGE) {
        URL.revokeObjectURL(state.resultItem.content);
      }

      // Resetting tab state
      state.selectedFile = "";
      state.openTab = 0;
      state.openTabId = resultTab.LIST;
      state.resultItem = { type: "", content: null };
      state.downloadFiles = [];
    },
    // Snackbar actions
    showAlert: (state, { payload }) => {
      const key = payload.options && payload.options.key;
      state.notifications = [
        ...state.notifications,
        {
          key: key || new Date().getTime() + Math.random(),
          ...payload,
        },
      ];
    },
    closeAlert: (state, { payload }) => {
      const dismissAll = !payload;
      state.notifications = state.notifications.map((notif) =>
        dismissAll || notif.key === payload
          ? { ...notif, dismissed: true }
          : { ...notif }
      );
    },
    removeAlert: (state, { payload }) => {
      state.notifications = state.notifications.filter(
        (notif) => notif.key !== payload
      );
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchJobs.pending, (state) => {
      state.loading = true;
      state.status = "loading";
    });
    builder.addCase(fetchJobs.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.status = "succeeded";

      // Sending sorted data based on key 'updatedAt'
      payload.sort((a, b) =>
        a.updatedAt < b.updatedAt ? 1 : a.updatedAt > b.updatedAt ? -1 : 0
      );

      const loadedJobs = payload.map((job, idx) =>
        createData(
          idx + 1,
          job.jobid,
          job.trainclassid,
          job.currentstation,
          job.nextstation,
          job.currentstate,
          job.route,
          job.updatedAt,
          job.stationmessages,
          job.visited
        )
      );

      // Added loaded jobs to the state
      state.jobs = loadedJobs;

      // To display and filter table rows
      state.rows = loadedJobs;
    });
    /**
     * Fetch All Jobs
     */
    builder.addCase(fetchJobs.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error.message;
    });
    /**
     * Fetch Stations
     */
    builder.addCase(fetchStations.fulfilled, (state, { payload }) => {
      state.status = "succeeded";
      state.stations = payload;
    });
    builder.addCase(fetchStations.rejected, (state, action) => {
      state.error = action.error.message;
    });
    /**
     * Fetch Job Result
     */
    builder.addCase(fetchJobResult.pending, (state) => {
      state.loading = true;
      state.status = "loading";
    });
    builder.addCase(fetchJobResult.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.status = "succeeded";
      state.jobResult = payload;
    });
    builder.addCase(fetchJobResult.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error;
      state.jobResult = [];
    });
    /**
     * Fetch Trains
     */
    builder.addCase(fetchTrains.pending, (state) => {
      state.loading = true;
      state.status = "loading";
    });
    builder.addCase(fetchTrains.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.status = "succeeded";
      state.trains = payload;
    });
    builder.addCase(fetchTrains.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error;
    });
    /**
     * Request Train
     */
    builder.addCase(requestTrain.pending, (state) => {
      state.loading = true;
      state.status = "loading";
    });
    builder.addCase(requestTrain.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.status = "succeeded";

      const updatedJobs = state.jobs.map((job) => ({ ...job, id: job.id + 1 }));
      updatedJobs.unshift(payload);

      state.jobs = updatedJobs;
      state.rows = updatedJobs;
      state.openRequestModal = false;
    });
    builder.addCase(requestTrain.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error;
    });
    builder.addCase(rejectTrain.pending, (state) => {
      state.loading = true;
      state.status = "loading";
    });
    builder.addCase(rejectTrain.fulfilled, (state) => {
      state.loading = false;
      state.status = "succeeded";
      state.openRejectModal = false;
    });
    builder.addCase(rejectTrain.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error;
    });
    builder.addCase(downloadResultItem.pending, (state) => {
      state.loading = true;
      state.status = "loading";

      // Releasing previous object to prevent memory leaks
      if (state.resultItem.type === resultItemSupportedTypes.IMAGE) {
        URL.revokeObjectURL(state.resultItem.content);
      }

      state.resultItem = { type: "", content: null };
    });
    builder.addCase(downloadResultItem.fulfilled, (state, action) => {
      state.loading = false;
      state.status = "succeeded";

      const { file, content } = action.payload;
      if (imageRegex.test(file)) {
        state.resultItem.type = resultItemSupportedTypes.IMAGE;
        state.resultItem.content = URL.createObjectURL(content);
      } else if (textFileRegex.test(file)) {
        state.resultItem.type = file.endsWith(".csv")
          ? resultItemSupportedTypes.CSV
          : resultItemSupportedTypes.TEXT;
        state.resultItem.content = content;
      }

      // Jump to View tab after fetching file contents
      state.openTab = 1;
      state.openTabId = resultTab.VIEW;
      state.selectedFile = file;
    });
    builder.addCase(downloadResultItem.rejected, (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.error = action.error;
    });
  },
});

/**
 * Selectors
 */
export const selectAllJobs = (state) => state.trains.jobs;
export const selectAllTrains = (state) => state.trains.trains;
export const selectAllStations = (state) => state.trains.stations;
export const selectJobResult = (state) => state.trains.jobResult;
export const getTableRows = (state) => state.trains.rows;
export const getJobsStatus = (state) => state.trains.status;
export const getJobsError = (state) => state.trains.error;
export const getOpenRequestModal = (state) => state.trains.openRequestModal;
export const getOpenRejectModal = (state) => state.trains.openRejectModal;
export const getNotifications = (state) => state.trains.notifications;
export const getDownloadFiles = (state) => state.trains.downloadFiles;
export const getOpenTab = (state) => state.trains.openTab;
export const getOpenTabId = (state) => state.trains.openTabId;
export const getSelectedFile = (state) => state.trains.selectedFile;
export const getResultItem = (state) => state.trains.resultItem;
export const isLoading = (state) => state.trains.loading;

/**
 * Actions
 */
export const {
  setTableRows,
  setRequestModal,
  setRejectModal,
  setDownloadFiles,
  setOpenTab,
  setSelectedFile,
  showAlert,
  closeAlert,
  removeAlert,
  resetResultTab,
} = trainsSlice.actions;

export default trainsSlice.reducer;
