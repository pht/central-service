import { useState, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";

import StyledButton from "../Button";
import StyledDialogTitle from "../DialogTitle";
import SelectTrain from "./SelectTrain";
import SelectRoute from "./SelectRoute";
import {
  requestTrain,
  isLoading,
} from "../../pages/train-requester/trainsSlice";

const TrainRequestModal = ({ open, handleClose }) => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);

  const [train, setTrain] = useState("");
  const [trainRoutes, setTrainRoutes] = useState([{ name: "", id: "" }]);

  const handleTrainChange = (event) => {
    setTrain(event.target.value);
  };

  const renderRoutes = () =>
    trainRoutes.map((rt, idx) =>
      idx === trainRoutes.length - 1 ? (
        rt.name
      ) : (
        <Fragment key={idx}>{`${rt.name} ⇒ `}</Fragment>
      )
    );

  const handleSubmitTrain = () => {
    if (!train || trainRoutes.some((route) => route.name === "")) {
      alert("Please enter all missing fields!");
    } else {
      const payload = {
        route: trainRoutes.map((rt) => rt.id).toString(),
        trainclassid: train,
        traininstanceid: 1,
      };

      dispatch(requestTrain(payload)).then(() => {
        setTrain("");
        setTrainRoutes([{ name: "", id: "" }]);
      });
    }
  };

  return (
    <Dialog fullWidth maxWidth="md" open={open}>
      <StyledDialogTitle onClose={handleClose} disable={loading}>
        Create Train Request
      </StyledDialogTitle>
      <DialogContent dividers>
        <SelectTrain
          loading={loading}
          selected={train}
          handleTrainChange={handleTrainChange}
        />
        <SelectRoute
          loading={loading}
          routes={trainRoutes}
          setRoutes={setTrainRoutes}
        />
        <Typography mt={3}>
          <b>Description:</b> None
        </Typography>
        <Typography>
          <b>Train:</b> {!train ? "Not Selected" : train}
        </Typography>
        <Typography>
          <b>Route:</b> {!trainRoutes[0].name ? "Not Selected" : renderRoutes()}
        </Typography>
      </DialogContent>
      <DialogActions>
        <StyledButton
          variant="contained"
          onClick={handleSubmitTrain}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} thickness={5} /> : "Submit"}
        </StyledButton>
      </DialogActions>
    </Dialog>
  );
};

export default TrainRequestModal;
