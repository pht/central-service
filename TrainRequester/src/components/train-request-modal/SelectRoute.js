import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

import CountAdornment from "../CountAdornment";
import { getStationId, groupStations } from "../../pages/train-requester/utils";

const SelectRoute = ({ routes, setRoutes, loading }) => {
  const grouped = groupStations();

  const handleAddRoute = () => {
    setRoutes([...routes, { name: "", id: "" }]);
  };

  const handleRemoveRoute = (idx) => {
    if (routes.length > 1) {
      routes.splice(idx, 1);
      setRoutes([...routes]);
    }
  };

  return (
    <Grid container spacing={2} sx={{ mt: 1 }}>
      {routes.map((rt, idx) => (
        <Grid item key={idx} xs={6} display="flex">
          <FormControl fullWidth size="small">
            <InputLabel htmlFor="route-select">Select Route</InputLabel>
            <Select
              id="route-select"
              label="Select Route"
              native
              disabled={loading}
              value={rt.name}
              onChange={(evt) => {
                routes[idx].name = evt.target.value;
                routes[idx].id = getStationId(evt.target.value);
                setRoutes([...routes]);
              }}
              startAdornment={<CountAdornment index={idx} />}
            >
              <option aria-label="None" value="" />
              {Object.keys(grouped).map((title) => (
                <optgroup key={title} label={title}>
                  {grouped[title].map((station) => (
                    <option
                      key={station.name}
                      value={station.name}
                      label={`${station.name} (${station.status})`}
                    />
                  ))}
                </optgroup>
              ))}
            </Select>
          </FormControl>
          <IconButton
            disabled={loading}
            sx={{ borderRadius: 2 }}
            aria-label="delete"
            color="primary"
            onClick={() => handleRemoveRoute(idx)}
          >
            <RemoveIcon />
          </IconButton>
          <IconButton
            disabled={loading}
            sx={{ borderRadius: 2 }}
            aria-label="add"
            color="primary"
            onClick={handleAddRoute}
          >
            <AddIcon />
          </IconButton>
        </Grid>
      ))}
    </Grid>
  );
};

export default SelectRoute;
