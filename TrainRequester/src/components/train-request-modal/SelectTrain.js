import * as React from "react";
import { useSelector } from "react-redux";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

import { getStationId } from "../../pages/train-requester/utils";
import { selectAllTrains } from "../../pages/train-requester/trainsSlice";

export default function SelectTrain({ selected, handleTrainChange, loading }) {
  const trains = useSelector(selectAllTrains);

  return (
    <FormControl fullWidth margin="normal">
      <InputLabel htmlFor="train-select">Select Train</InputLabel>
      <Select
        id="train-select"
        label="Select Train"
        native
        disabled={loading}
        value={selected}
        onChange={handleTrainChange}
      >
        <option aria-label="None" value="" />
        {trains.map((train) => (
          <optgroup key={train.name} label={train.name}>
            {train.tags.map((tag) => (
              <option key={tag.id} value={`${train.name}:${tag.name}`}>
                {`${train.name}:${tag.name}`}
              </option>
            ))}
          </optgroup>
        ))}
      </Select>
    </FormControl>
  );
}
