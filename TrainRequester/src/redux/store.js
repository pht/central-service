import { configureStore } from "@reduxjs/toolkit";

import trainsReducer from "../pages/train-requester/trainsSlice";

export default configureStore({
  reducer: {
    trains: trainsReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoreActions: ["trains/downloadResultItem/fulfilled"],
      },
    }),
});
