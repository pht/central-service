/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Errors', 'model/Icon'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/Errors'), require('../model/Icon'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.IconApi = factory(root.HarborApi.ApiClient, root.HarborApi.Errors, root.HarborApi.Icon);
  }
}(this, function(ApiClient, Errors, Icon) {
  'use strict';

  /**
   * Icon service.
   * @module api/IconApi
   * @version 2.0
   */

  /**
   * Constructs a new IconApi. 
   * @alias module:api/IconApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getIcon operation.
     * @callback module:api/IconApi~getIconCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Icon} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get artifact icon
     * Get the artifact icon with the specified digest. As the original icon image is resized and encoded before returning, the parameter \"digest\" in the path doesn't match the hash of the returned content
     * @param {String} digest The digest of the resource
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {module:api/IconApi~getIconCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Icon}
     */
    this.getIcon = function(digest, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'digest' is set
      if (digest === undefined || digest === null) {
        throw new Error("Missing the required parameter 'digest' when calling getIcon");
      }


      var pathParams = {
        'digest': digest
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = Icon;

      return this.apiClient.callApi(
        '/icons/{digest}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
