/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ChartAPIError'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ChartAPIError'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.NotFoundChartAPIError = factory(root.HarborApi.ApiClient, root.HarborApi.ChartAPIError);
  }
}(this, function(ApiClient, ChartAPIError) {
  'use strict';

  /**
   * The NotFoundChartAPIError model module.
   * @module model/NotFoundChartAPIError
   * @version 2.0
   */

  /**
   * Constructs a new <code>NotFoundChartAPIError</code>.
   * Not found
   * @alias module:model/NotFoundChartAPIError
   * @class
   * @extends module:model/ChartAPIError
   * @param error {String} The error message returned by the chart API
   */
  var exports = function(error) {
    ChartAPIError.call(this, error);
  };

  /**
   * Constructs a <code>NotFoundChartAPIError</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/NotFoundChartAPIError} obj Optional instance to populate.
   * @return {module:model/NotFoundChartAPIError} The populated <code>NotFoundChartAPIError</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      ChartAPIError.constructFromObject(data, obj);
    }
    return obj;
  }

  exports.prototype = Object.create(ChartAPIError.prototype);
  exports.prototype.constructor = exports;

  return exports;

}));
