module.exports = {
    development: {
      //If you want to use the center_database instead of the one create for your system user, 
      //Replace the environment variables below
      username: [Censored],
      password: [Censored],
      database: [Censored],
      host: [Censored],
      dialect: [Censored],
    },
    test: {
        username: [Censored],
        password: [Censored],
        database: [Censored],
        host: [Censored],
        dialect: [Censored],
    },
    production: {
        username: [Censored],
        password: [Censored],
        database: [Censored],
        host: [Censored],
        dialect: [Censored],
    },
  };