const express = require('express');
var router = express.Router();

const testController = require('../controllers').test;
const jobinfoController = require('../controllers').jobinfo;
const jobResultController = require('../controllers').jobresult;
const harborController = require('../controllers').harbor;
const stationController = require('../controllers').station;

const harborUtil = require('../utils').harbor;

module.exports = (keycloak)=>{

  router.get('/', function(req, res, next) {
    res.send('API of the Central PHT Service');
  });
  
  /* TEST Router */
  router.get('/test', keycloak.protect(), testController.list);
  router.get('/test/:id', testController.getById);
  router.post('/test', testController.add);
  router.put('/test/:id', testController.update);
  router.delete('/test/:id', testController.delete);
  
  /* JOBINFO Router */
  router.get('/jobinfo', keycloak.protect(), jobinfoController.list);
  router.get('/jobinfo/:id', keycloak.protect(), jobinfoController.listPullableTrains);
  router.post('/jobinfo', keycloak.protect(), harborUtil.auth, jobinfoController.add);
  router.post('/jobinfo/skipCurrentStation', keycloak.protect(), harborUtil.auth, jobinfoController.skipCurrentStation);

  /* JOBRESULT Router */
  router.get('/jobresult/:id', keycloak.protect(), jobResultController.view);
  router.get('/jobresult/:id/download', keycloak.protect(), jobResultController.download);

  /* HARBOR Router */
  router.get('/harbor/projects', keycloak.protect(), harborUtil.auth, harborController.getProjects);
  router.get('/harbor/trains', keycloak.protect(), harborUtil.auth, harborController.getTrainClassRepositories);


  /* STATION ROUTER */
  router.get('/stations', keycloak.protect(), stationController.getStations);
  router.post('/stations/onboard', stationController.onboardStation);
  router.post('/stations/publickey', keycloak.protect(), stationController.storeStationPublicKey);
  router.get('/stations/publickey', stationController.getStationPublicKey);

  return router;
};
