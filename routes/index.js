const express = require('express');
var router = express.Router();

module.exports = (keycloak) => {

  router.get('/userinfo', keycloak.protect(), function (req, res, next) {
    let userinfo = {};
    userinfo.email = req.kauth.grant.access_token.content.email;
    userinfo.preferred_username = req.kauth.grant.access_token.content.preferred_username;
    return res.status(200).send(userinfo);
  });

  return router;
};
