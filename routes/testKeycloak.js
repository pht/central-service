const request = require("request");
const express = require("express");
var router = express.Router();


function parseCookies(request) {
    var list = {},
        rc = request.headers["set-cookie"][1];
    console.log(rc);
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}


module.exports = (keycloak) => {

    router.get("/", keycloak.protect(), function (req, res, next) {

        console.log(JSON.stringify(req.kauth.grant, undefined, 2));
        


        var options = {
            method: 'GET',
            url: "[Censored]",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${req.kauth.grant.access_token.token}`,
                'Accept': 'application/vnd.scanner.adapter.vuln.report.harbor+json; version=1.0'
            }
        };

        //console.log(options);

        request(options, function (error, response, body) {
            //console.log("err", error);
            console.log("res1", JSON.stringify(response.headers, undefined, 2));
            console.log("bod1", JSON.stringify(body));

            options.headers["X-Xsrftoken"] = Buffer.from(parseCookies(response)["_xsrf"].split("|")[0], 'base64').toString();
            console.log(options)
            if (res.statusCode == 403) {
                request(options, function (error, response, body) {
                    //console.log("err", error);
                    //console.log("res", response);
                    console.log("bod2", JSON.stringify(body));
                });
            }

        });

        res.json(req.kauth.grant.access_token.content);
    });

    return router;
};
