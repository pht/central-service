const request = require('request')
const _ = require('lodash')
const harborUtil = require('../utils').harbor;

module.exports = {

    async getProjects(req, res, next) {

        let harborApiClient = harborUtil.getHarborApiClient(req);

        //GET PROJECTS - USER LEVEL ACCESS
        try {
            let harborProjectApi = new harborApiClient.ProjectApi();
            let getProjectResult = await harborProjectApi.listProjects();
            console.log(getProjectResult.body);
            let projects = getProjectResult.body;
            
            //GET REPOSITORIES FOR EACH PROJECT
            let repoPromises = [];
            _.forEach(projects, (project) => {
                let RepositoryApi = new harborApiClient.RepositoryApi();
                repoPromises.push(RepositoryApi.listRepositories(project.name));
            });

            let repoPromisesResult = await Promise.all(repoPromises);
            let repositories = repoPromisesResult.map(a => a.body);
            repositories.map((repo, i) => {
                if (!repo)
                    repo = [];
                projects[i].repositories = repo;
            });

            let tagPromises = [];
            _.forEach(projects, (project) => {
                _.forEach(project.repositories, (repo) => {
                    let ArtifactApi = new harborApiClient.ArtifactApi();
                    var opts = { 'withTag': true };
                    let projectName = project.name;
                    let repoName = repo.name.replace(/^.+?\//, '');
                    tagPromises.push(ArtifactApi.listArtifacts(projectName, repoName, opts));
                });
            });

            tagPromisesResult = await Promise.all(tagPromises);
            let artifacts = tagPromisesResult.map(a => a.body);
            _.forEach(projects, (project) => {
                _.forEach(project.repositories, (repo, i) => {
                    let repoArtifacts = artifacts[i] || [];
                    repo.tags = repoArtifacts.map(a => a.tags)
                    repo.tags = _.flatten(repo.tags);
                });
            });

            res.status(200).send(projects);

        } catch (error) {
            console.error(error);
            next(error);
        }
    },

    async getTrainClassRepositories(req, res, next) {

        let train_class_repository = "train_class_repository";
        let harborApiClient = harborUtil.getHarborApiClient(req, true);

        //GET ALL REPOSITORIES FROM train_class_repository
        let repositories = [];
        try {
            let RepositoryApi = new harborApiClient.RepositoryApi();
            let opts = { 'pageSize': 0 };
            let result = await RepositoryApi.listRepositories(train_class_repository, opts);
            repositories = result.body;
        } catch (error) {
            console.log(error);
            next(error);
        }

        let repoPromises = [];
        _.forEach(repositories, async (repo) => {

            // console.log(repo.name);
            //GET ARTIFACTS FOR EACH REPOSITORY (With Tags)
            let ArtifactApi = new harborApiClient.ArtifactApi();
            var opts = { 'withTag': true };
            let projectName = repo.name.split("/")[0];
            let repoName = repo.name.replace(/^.+?\//, '');
            repoPromises.push(ArtifactApi.listArtifacts(projectName, repoName, opts));
        });

        let promiseResult = [];
        try {
            promiseResult = await Promise.all(repoPromises);
        } catch (error) {
            console.log(error);
            next(error);
            return;
        }

        let artifacts = promiseResult.map(a => a.body);
        _.forEach(repositories, (repo, i) => {
            let repoArtifacts = artifacts[i];
            repo.tags = repoArtifacts.map(a => a.tags)             
            repo.tags = _.compact(_.flatten(repo.tags));
        });

        res.status(200).send(repositories);

    }

}