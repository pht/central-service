const { asyncHandler } = require('../utils').asyncHandler;
const JobInfo = require('../models').jobinfo;
const _ = require('lodash')
const tar = require("tar");
const tarStream = require("tar-stream");
const fs = require('fs');
const trainConfigUtil = require('../utils').trainConfig;
const fileSystem = require('../utils').fileSystem;
const vault = require('../utils').vault;
const docker = require('../utils').docker;
const cryptoUtil = require('../utils').crypto;
const path = require('path');
const { Readable } = require("stream")

const getResultSymmetricKeyName = (jobId) => {
    const symmetricKeyName = jobId + "_results";
    return symmetricKeyName;
}

//Returns the job from DB
const getJob = (jobId, userId) => 
{
    return new Promise(async (resolve, reject) => {
        //Get Job from DB
        try{
            let job = await JobInfo.findOne({
                where: {
                    userid: userId,
                    jobid: jobId, 
                    currentstate: 'finished'
                },
                order: [
                    ['createdAt', 'DESC']
                ]
            }); 
            if(job === null)
            {
                return reject("Could not find finsihed job with id: " + jobId + " for user " + userId);
            }
            resolve(job);
        } catch(error)
        {
            console.log(error);
            return reject(error);
        }
    }); 
}

//Extracts train_config and file.enc from image, returns trainconfig object
const extractFromImage = (container, tempDir) => 
{
    return new Promise(async (resolve, reject) => 
    {
        //Two files needed: 
        //- traing_config json containing a encrypted symmetric key
        //- file.enc an encrypted tar archive containing all the changes
        const trainConfigFilePathInContainer = trainConfigUtil.getTrainConfigFilePathInContainer();
        const encryptedTarArchiveFilePathInContainer = fileSystem.getEncryptedTarArchiveFilePathInContainer();

        let trainConfig;
        try {

            //get tared train config, then untar
            let trainConfigArchive = await docker.extractFileAsArchive(container, trainConfigFilePathInContainer);
            trainConfig = await trainConfigUtil.unTarTrainConfigJson(trainConfigArchive);
        } catch(err)
        {
            return reject(err);
        }

        //Extract encrypted archive
        try{
           
            let encryptedArchive = await docker.extractFileAsArchive(container, encryptedTarArchiveFilePathInContainer); 
            await new Promise(fulfill => {
                encryptedArchive.pipe(
                    tar.extract({
                        cwd: tempDir,
                        sync: true,
                    })
                ).on("finish", fulfill)
            });            
            
        } catch(err)
        {
           return reject(err);
        }
        return resolve(trainConfig);
    }); 
}

//Main Method, extracts information from the container for the job and stores it at a temp path
const storeEncryptedArchiveToTempPath = (jobId, userId) =>
{
    return new Promise(async (resolve, reject) => {

        let tempDir = fileSystem.getTempDirPath(jobId);
        const encryptedTarArchiveFileName = fileSystem.getEncryptedTarArchiveFileName();
        const encryptedTarArchivePath = path.join(tempDir, encryptedTarArchiveFileName); 

        //Get the job object
        let job; 
        try
        {
            job = await getJob(jobId, userId);
        } catch(error)
        {
            console.log(error); 
            return reject(error);
        }        
        if(fs.existsSync(encryptedTarArchivePath)) 
        {
            //No need to reextract, files have been already extracted
            //Return path to encrypted archive
            console.log("Extracted archive exists, will not extract again");
            return resolve(encryptedTarArchivePath);
        } else
        {
            console.log("Extracting from job");
            tempDir = fileSystem.getTempDir(jobId, true);
        }
        let tempContainer; 
        try{
            
            //Nothing has been extracted yet, extract infos from image
            tempContainer = await docker.createContainer(job.trainstoragelocation + ":" + job.currentstation);
            let trainConfig = await extractFromImage(tempContainer, tempDir);
    
            //Get symmetric key for decryption
            if(!trainConfig.hasOwnProperty(trainConfigUtil.train_config_constant["symmetric_key"]) || trainConfig[trainConfigUtil.train_config_constant["symmetric_key"]] === "")
            {
                let err = "Symmetric Key not found in config. Cannot decrypt";
                return reject(err);
            }
    
            //Decrypt symmetric key
            const encryptedSymmetricKey = trainConfig[trainConfigUtil.train_config_constant["symmetric_key"]];
            const decryptedSymmetricKey = await trainConfigUtil.decryptSymmetricKey(encryptedSymmetricKey, jobId);
    
            // restore symmetric key to vault (so that we can use vault for the decryption)
            // Restore here means we add an existing keys as a new named key in vault
            const symmetricKeyName = getResultSymmetricKeyName(jobId)
            const symmetricKeyBackup = cryptoUtil.trainConfig.getVaultSymmetricKeyBackupModel(symmetricKeyName, decryptedSymmetricKey, null, { isBase64: true });
            await vault.instance.write(`transit/restore/${symmetricKeyName}`, {
                backup: symmetricKeyBackup,
                name: symmetricKeyName,
                // force the restore to proceed even if a key by this name already exists.
                force: true
            });
    
            //Return the archive path as a result
            console.log("extraction succcessfull");
            return resolve(path.join(tempDir, encryptedTarArchiveFileName));
        } catch(err) {
            console.log("Error while extracting");
            return reject(err);
        } finally{
            //Remove the temp container
            if(tempContainer)
            {
                tempContainer.remove().catch((err) => { reject(err);});
            }
        }
    });
}

const decryptTarArchive = (jobId, encryptedTarArchiveFilePath) => 
{
    return new Promise(async (resolve, reject) => {

        //Decrypt tar archive, return as file stream
        const ciphertext = fs.readFileSync(encryptedTarArchiveFilePath, "utf8");
        let decryptionResult; 
        try {
            //Use the named vault key restored beforehand for decryption
            decryptionResult = await vault.instance.write(`transit/decrypt/${getResultSymmetricKeyName(jobId)}`, { ciphertext: ciphertext });
        } catch(err)
        {
            console.log(err); 
            return reject(err);
        }        
        const plaintextEncoded = decryptionResult.data.plaintext;

        // base64 decode - a tar archive file
        return resolve(Buffer.from(plaintextEncoded, 'base64'));
    });
}

//Creates tree from the tar changes array
//Credit: https://stackoverflow.com/a/57344801/5589776
const createDataTree = dataset => {
    let result = [];
    let level = {result};
    
    dataset.forEach(path => {
      path.split('/').reduce((r, name, i, a) => {
        if(!r[name]) {
          r[name] = {result: []};
          r.result.push({name, path: a.slice(0,i+1).join('/'), children: r[name].result })
        }
        
        return r[name];
      }, level)
    });
    return result;
};

//Extracts the file at the given path from the archive
const extractFileFromArchive = (archivePlainText, filePath) =>
{
    return new Promise(async (resolve, reject) => {
        try{
            const plaintextStream = Readable.from(archivePlainText);
            let extractContents = tarStream.extract();
            var data = [];
            extractContents.on('entry', function (header, stream, next) {
        
                //Read file content
                stream.on('data', function(chunk) {
                if (header.name == filePath)
                    data.push(chunk);
                });
                
                //Get next element
                stream.on('end', function() {
                    next();
                });
        
            }).on('finish', function () {
                resolve(data);
            });
            plaintextStream.pipe(extractContents);
        } catch(err)
        {
            reject(err);
        }
    });   
}

module.exports = {

    //Returns the contents of the jobresult as json
    view : asyncHandler(async (req, res, next) => {

        const jobId = req.params.id;
        const userId = req.kauth.grant.access_token.content.preferred_username;

        console.log("Fetching job results for " + jobId + " and user " + userId);
        let encryptedTarArchiveFilePath; 
        try
        {
            encryptedTarArchiveFilePath = await storeEncryptedArchiveToTempPath(jobId, userId); 
        } catch(err)
        {
            console.log("View: Error whole extracting archive for job " + jobId);
            console.log(err); 
            res.status(400).send(err);
            return; 
        };
       
        let plaintext; 

        try {
            
            //Decrypt Archive
            plaintext = await decryptTarArchive(jobId, encryptedTarArchiveFilePath); 

        } catch (err)
        {
            console.log("View: error while decrypting archive for job " + jobId);
            console.log(err); 
            res.status(400).send(err);
            return;
        }

        //Extract changes
        let changes = [];

        const plaintextStream = Readable.from(plaintext);
        let extractListOfContents = tarStream.extract();
        extractListOfContents.on('entry', function (header, stream, next) {

            // header is the tar header
            // stream is the content body (might be an empty stream)
            // call next when you are done with this entry

            // clear stream - just file name is enough
            if (header.type == "file") {
                changes.push(header.name);
                stream = Readable.from("");
            }

            stream.on('end', function () {
                next() // ready for next entry
            })

            stream.resume(); // just auto drain the stream
    
        }).on('finish', function () {
            res.status(200).send(JSON.stringify(createDataTree(changes))); 
        });
        plaintextStream.pipe(extractListOfContents);
    }),

    //Provides a stream to download the .tar archive
    download : asyncHandler(async (req, res, next) => {

        const jobId = req.params.id;
        const userId = req.kauth.grant.access_token.content.preferred_username;

        console.log("Downloading jobresults for job " + jobId + " and user " + userId);
        let encryptedTarArchiveFilePath; 
        try
        {
            encryptedTarArchiveFilePath = await storeEncryptedArchiveToTempPath(jobId, userId); 
        } catch(err)
        {
            console.log("Download: Error while extracting archive for job " + jobId);
            console.log(err); 
            res.status(400).send(err);
            return; 
        };
       
        try {
            
            //Decrypt Archive
            let plaintext = await decryptTarArchive(jobId, encryptedTarArchiveFilePath); 

            //Check if whole archive or single path should be downloaded    
            if (typeof req.query.path !== 'undefined' && !(req.query.path  === ""))
            {
                console.log("Downloading file " + req.query.path + " for " + jobId + " and user " + userId);

                let fileData = await extractFileFromArchive(plaintext, req.query.path); 
                if(fileData === undefined || fileData.length == 0)
                {
                    console.log("Could not find requested file " + req.query.path);
                    res.status(404).send("File not found");
                    return;
                }

                console.log("File " + req.query.path + " successfully extracted");
                //Write file as response
                res.writeHead(200, {
                    "Content-Type": "application/octet-stream",
                    "Content-Disposition": "attachment; filename=" + req.query.path.split('/').pop()
                });

                Readable.from(fileData).pipe(res);

            } else{
                //Write whole archive as response
                res.writeHead(200, {
                    "Content-Type": "application/octet-stream",
                    "Content-Disposition": "attachment; filename=result" + jobId + ".tar"
                });

                Readable.from(plaintext).pipe(res);
            }
        } catch (err)
        {
            console.log("Download: error while decrypting/extracting archive for job " + jobId)
            console.log(err); 
            res.status(400).send(err);
        }
    })
}