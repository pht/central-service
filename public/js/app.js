angular.module('app', ['ui.bootstrap', 'ui-notification']);
angular.module('app').config(function ($provide, $httpProvider) {
    
    $httpProvider.interceptors.push('HttpInterceptor');

    $provide.decorator('$exceptionHandler', function ($delegate) {
        return function (exception, cause) {

            console.log('exception',exception, cause)
            $delegate(exception, cause);

        };
    });

});

const InterceptorSkipHeader = 'X-Skip-Interceptor';

angular.module('app').factory('HttpInterceptor', function ($q, $location, $injector) {

    return {

        'request': function(config) {
            // config
            // console.log('request,config',config);

            // doesn't manipulate template urls
            if (config.url.indexOf(".html") > -1)
                return config;


            config.url = (HOST_BASE ? `/${HOST_BASE}` : '') + config.url;
            return config;

          },

          'response': function(response) {
            // on success
            //console.log('response',response);
            return response;
          },
      
        'responseError': function (rejection) {
            // on error

            console.log('rejection', rejection);

            let Notification = $injector.get('Notification');

            // Handle Token Expiration
            if (rejection.data === null && rejection.status === -1 && rejection.xhrStatus === "error") {
                Notification.error({ message: 'Token expired.' });
                window.location = window.origin;
            } else if (rejection.config.headers['InterceptorSkipHeader']) {

                console.log("Skipping http interception because of present skip header");
                return $q.reject(rejection);
            } else {
                Notification.error({ message: 'Something went wrong.' });
            }
                
            return $q.reject(rejection);
        }
    };

});
angular.module('app').factory('authService', function ($q, localStorageService, $http) {
    var _service = {};
    var _authentication = {};

    var _getUserInfo = function () {

        var deferred = $q.defer();
        $http.get("/userinfo").then(function (response) {
            debugger;
            if (response.data) {
                localStorageService.set('userinfo', response.data.data);
                _authentication.isAuth = true;
                _authentication.user = response.data.data.user;

                deferred.resolve(response.data);
            }
        }, function (error) {
            debugger;
            deferred.reject(error.data);
            // localStorageService.set('authorizationData', 'null');
            // _authentication.isAuth = false;
            // _authentication.user = null;
        });

        return deferred.promise;
    }

    _service.getUserInfo = _getUserInfo;

    return _service;
});
angular.module('app').controller('navBarController', function($scope, $http){

    $http.get("/userinfo").then(function (response) {
        console.log("userinfo", response.data);
        $scope.userInfo = response.data;
    });

});
angular.module('app').controller('testController', function ($scope, $http, $timeout, $uibModal) {

    //Initial job list filter
    $scope.jobListFilter = [];

    function flattenTrainClassList(trainClassList) {

        let trainClassListFlattened = [];
        _.forEach(trainClassList, (repo) => {
            _.forEach(repo.tags, (tag) => {
                
                if (tag == null) return;
                let item = angular.copy(repo);
                tagKeys = Object.keys(tag);
                _.forEach(tagKeys, (key) => {
                    item[`tag_${key}`] = tag[key];
                });

                trainClassListFlattened.push(item);
            })
        });

        $scope.trainClassListFlattened = trainClassListFlattened;
        return trainClassListFlattened;

    }

    $scope.getProjectList = function (callback) {
        $http.get('/api/harbor/projects').then(function (res) {
            console.log(res)

            $scope.projectList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });
    }

    $scope.getTrainClassList = function (callback) {

        $http.get('/api/harbor/trains').then(function (res) {
            console.log(res)

            $scope.trainClassList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.getJobList = function (callback) {
        //Get job info with the provided filter
        $http.get('/api/jobinfo?statusFilter=' + $scope.jobListFilter.join(",")).then(function (res) {
            console.log(res)

            $scope.jobList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.setJobListFilterAll = function () {
        $scope.jobListFilter = []
        $scope.refreshJobTable();
    }

    $scope.setJobListFilterFinished = function () {
        $scope.jobListFilter = ['finished'];
        $scope.refreshJobTable();
    }

    $scope.setJobListFilterRunning = function () {
        $scope.jobListFilter = ['wait_for_pull', 'pulled'];
        $scope.refreshJobTable();
    }

    $scope.downloadJobResult = function (jobId) {
        console.log("Downloading job result" + jobId);
    }

    $scope.getStationList = function (callback) {

        if($scope.stationList && $scope.stationList.length){
            if (callback)
                callback($scope.stationList);
            return;
        }

        $http.get('/api/stations').then(function (res) {
            console.log(res)

            $scope.stationList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.jobTableToDataTable = function () {

        let options = {
            year: '2-digit',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        }

        _.forEach($scope.jobList, (item) => {

            item.updatedAtReadable = new Date(item.updatedAt).toLocaleString('de-DE', options);

        });

        if ($scope.jobTable) {
            $scope.jobTable.destroy();
        }

        let timeout = $timeout(function () {

            $scope.jobTable = $('#jobTable').DataTable({

                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],

                //stateSave: true,

                // order by last update
                "order": [[7, "desc"]],
                "responsive": true,
                "destroy": true,

            });


            $scope.jobTable.on('order.dt search.dt', function () {
                $scope.jobTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

        }, 1);

    }

    //$scope.getProjectList();
    //$scope.getTrainClassList(flattenTrainClassList);
    $scope.getStationList($scope.getJobList($scope.jobTableToDataTable));

    $scope.refreshJobTable = function () {
        $scope.getStationList($scope.getJobList($scope.jobTableToDataTable));
    }

    $scope.openTrainRequestModal = function () {

        $scope.getTrainClassList((returnedData) => {
            flattenTrainClassList(returnedData);

            $scope.getStationList(()=>{

                let modalInstance = $uibModal.open({
                    templateUrl: 'trainRequestModal.html',
                    controller: 'trainRequestModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return {
                                trainClassListFlattened: $scope.trainClassListFlattened,
                                stationList: $scope.stationList,
                                findStationById: $scope.findStationById,
                                getStationNameById: $scope.getStationNameById
                            };
                        }
                    }
                });
    
                modalInstance.result.then(function (data) {
    
                    $scope.refreshJobTable();
    
                }, function () {
                    console.log("dismiss");
                });

            });

        });

    }

    $scope.openTrainRejectHandlerModal = function (job) {

        let modalInstance = $uibModal.open({
            templateUrl: 'trainRejectHandlerModal.html',
            controller: 'trainRejectHandlerModalCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return {
                        job: job,
                        getStationNameById: $scope.getStationNameById
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            $scope.refreshJobTable();

        }, function () {
            console.log("dismiss");
        });

    }

    $scope.openJobResultsModal = function (jobId) {

        let modalInstance = $uibModal.open({
            templateUrl: 'jobResultsModal.html',
            controller: 'jobResultsModalCtrl',
            size: 'lg',
            backdrop: 'static', 
            resolve: {
                data: function () {
                    return {
                        jobId: jobId
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            $scope.refreshJobTable();

        }, function () {
            console.log("dismiss");
        });

    }

    $scope.findStationById = function (stationId) {
        let station = _.find($scope.stationList, { 'id': stationId });
        return station;
    }

    $scope.getStationNameById = function (stationId) {
        let stationName = stationId;
        let station = $scope.findStationById(stationId);
        if (station) {
            stationName = station.name;
        }
        return stationName;
    }

});
angular.module('app').controller('trainRequestModalCtrl', function ($uibModalInstance, $scope, $http, Notification, data) {

    $scope.trainClassListFlattened = data.trainClassListFlattened;
    $scope.stationList = data.stationList;
    $scope.findStationById = data.findStationById;
    $scope.getStationNameById = data.getStationNameById;
    $scope.submitClicked = false;

    $scope.trainRequestData = {route:[null]};

    $scope.addStation = function(index){
        $scope.trainRequestData.route.splice(index+1, 0, null);
    }

    $scope.removeStation = function(index){

        $scope.trainRequestData.route.splice(index, 1);
        if($scope.trainRequestData.route.length == 0)
            $scope.addStation(0);

    }

    $scope.makeTrainRequest = function () {

        let reqData = {};
        reqData.trainclassid = `${$scope.trainRequestData.trainclass.name}:${$scope.trainRequestData.trainclass.tag_name}`;
        reqData.traininstanceid = 1;
        reqData.route = $scope.trainRequestData.route.join(",");
        //Submitted has been clicked
        $scope.submitClicked = true;

        $http.post('/api/jobinfo', reqData, {headers: {InterceptorSkipHeader : 'skip'}}).then(function (res) {
            if (res.status === 201) {

                Notification.success('The train request has been successfully sent.');
                $uibModalInstance.close({ data: res.data });

            } else
            {
                $scope.submitClicked= false;
                Notification.error({ message: 'Job could not be created, please look at the log files' });
            }


        }, function (error) 
        {
            $scope.submitClicked = false;
            console.log(error);
            Notification.error({ message: 'Job could not be created, please look at the log files' });
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
    
});
angular.module('app').controller('trainRejectHandlerModalCtrl', function ($uibModalInstance, $scope, $http, Notification, data) {

    $scope.job = data.job;
    $scope.getStationNameById = data.getStationNameById;
    $scope.stationMessage = $scope.job.stationmessages[_.indexOf($scope.job.visited, $scope.job.currentstation)];

    $scope.skipCurrentStation = function (isAll = false) {

        var result = confirm("Are you sure?");
        if (!result) {
            return;
        }

        let reqData = {isAll : isAll};
        reqData.jobID = $scope.job.jobid;

        $http.post('/api/jobinfo/skipCurrentStation', reqData).then(function (res) {

            console.log("res", res);
            Notification.success('The request has been successfully sent.');
            $uibModalInstance.close();

        });

    }

    $scope.getTooltipText = function (isAll = false) {
        let tooltipText = "The next stations will be";
        let currentStationIndex = _.indexOf($scope.job.visited, $scope.job.currentstation);
        let currentJobVisited = angular.copy($scope.job.visited);

        if (isAll)
            for (i = 0; i < currentJobVisited.length; i++) {
                if (currentJobVisited[i] == $scope.job.currentstation)
                    currentJobVisited[i] = -2;
            }
        else
            currentJobVisited[currentStationIndex] = -2;

        let remainingStations = _.filter(currentJobVisited, (station, index) => {
            if (index < currentStationIndex || station == -2)
                return false
            return true;
        });

        tooltipText = `${tooltipText} [${remainingStations.map(x => $scope.getStationNameById(x)).join(",")}].`;
        return tooltipText;
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});


angular.module('app').controller('jobResultsModalCtrl', function ($uibModalInstance, $scope, $http, $uibModal, Notification, data) {

    $scope.jobId = data.jobId;
    $scope.loading = true; 
    $scope.error = false;
    $scope.jobResults = []; 
    $scope.selectedFiles = [];

    //Get content 
    $http.get("/api/jobresult/" + data.jobId, {headers: {InterceptorSkipHeader : 'skip'}}).then(function (response) {
        if (response.data) {
            try{
                //display the job results
                response.data.forEach(element => $scope.jobResults.push(element));
                $scope.loading = false;
            }catch(err)
            {
                console.log(err);
                $scope.error = true;
                $scope.loading = false;
                Notification.error({ message: 'Could not load job results' });
            }
        }
    }, function (error) {
        $scope.error = true;
        $scope.loading = false;
        console.log(error);
        Notification.error({ message: 'Could not load job results' });
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
    
    
    $scope.openJobResultItemModal = function (item) {
        const modalInstance = $uibModal.open({
            templateUrl: 'jobResultItemModal.html',
            controller: 'jobResultItemModalCtrl',
            size: 'lg',
            backdrop: 'static',
            windowClass: 'modal-dialog-scrollable',
            resolve: {
                data: function () {
                    return {
                        item: item,
                        jobId: data.jobId
                    };
                }
            }
        });

        modalInstance.result.then(function (_) {
            $scope.refreshJobTable();
        }, function () {
            console.log("dismiss");
        });
    };

    $scope.downloadSelectedFiles = function () {
        let download = document.createElement('a');
        download.setAttribute('download', null);
        download.style.display = 'none';
        document.body.appendChild(download);
        $scope.selectedFiles.forEach(file => {
            download.setAttribute('href', `api/jobresult/${$scope.jobId}/download/?path=${file}`);
            download.click();
        });
        document.body.removeChild(download);
    }

    $scope.toggleFileCheck = function (file) {
        if ($scope.selectedFiles.indexOf(file) === -1) {
            $scope.selectedFiles.push(file);
        } else {
            $scope.selectedFiles.splice($scope.selectedFiles.indexOf(file), 1);
        }
    }
});

angular.module('app').controller('jobResultItemModalCtrl', function ($uibModalInstance, $scope, $http, Notification, data) {
    $scope.jobId = data.jobId;
    $scope.item = data.item;
    $scope.loading = true; 
    $scope.error = false;
    $scope.visualizationTypes = [ 'line', 'scatter', 'bar'];
    $scope.selectedVisualization = 'scatter';
    
    const imageExtensionRegex = (/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i); 
    const textFileExtensionRegex = (/\.(txt|csv)$/i);

    if (imageExtensionRegex.test(data.item.path)) {
        $scope.isImage = true;
        $scope.loading = false;
    } else if (textFileExtensionRegex.test(data.item.path)) {
        $scope.isFile = true;
        $http
        .get(`/api/jobresult/${data.jobId}/download/?path=${data.item.path}`, {headers: {InterceptorSkipHeader : 'skip'}})
        .then( function(response) {
            if (response.data) {
                try {
                    if (data.item.path.endsWith('.csv')) {
                        const data = Papa.parse(response.data, {header: true, skipEmptyLines: true});
                        $scope.isCsv = true;
                        $scope.csvHeaders = data.meta.fields;
                        $scope.csvData = data.data;
                    } else {
                        $scope.isText = true;
                        $scope.textContents = response.data;
                    }                  
                    $scope.loading = false;
                } catch (error) {
                    $scope.error = true;
                    $scope.loading = false;
                    console.log(`Error fetching job result item : ${error}`);
                    Notification.error({ message: 'Could not load job result item' });
                }
            } 
        }, function (error) {
            $scope.error = true;
            $scope.loading = false;
            console.log(`Error fetching job result item : ${error}`);
            Notification.error({ message: 'Could not load job result item' });
        });
    } else {
        $scope.isUnsupportedFormat = true;
        $scope.loading = false;
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.buildPlot = function(field1, field2, vizType) {
        const colors = ['rgb(255, 99, 132)', 'rgb(54, 162, 235)', 'rgb(75, 192, 192)', 'rgb(153, 102, 255)','rgb(255, 159, 64)'];
        const backgroundColors = ['rgb(255, 99, 132, 0.2)', 'rgb(54, 162, 235, 0.2)', 'rgb(75, 192, 192, 0.2)', 'rgb(153, 102, 255, 0.2)','rgb(255, 159, 64, 0.2)'];
        const colorIndex = Math.floor(Math.random() * 5);
        const chartStatus = Chart.getChart('plotCanvas');
        if (chartStatus != undefined) {
            chartStatus.destroy();
        }

        const ctx = $('#plotCanvas');
        const dataset = _.map($scope.csvData, (o) => {return {x: o[field1], y: o[field2]}});
        const chartData = {
            datasets: [{
                label: `${field1} vs ${field2}`,
                data: dataset,
                backgroundColor: backgroundColors[colorIndex],
                borderColor: colors[colorIndex],
                borderWidth: 1
            }]
        };
        const vizChart = new Chart(ctx, {
            type: vizType,
            data: chartData,
            options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: field1
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: field2
                        }
                    }
                }
            }
        });
    }
});

angular.module('app').filter('filterPrevSelectedStation', [function () {
    return function (list, index, selected) {

        if (index == 0)
            return list;
        if (selected[index - 1] == null)
            return list;

        var array = angular.copy(list);

        _.remove(array, { name: selected[index - 1] });

        return array;
    };
}]);