<table>

<tr>
<td>
![CentralServiceLogo](/uploads/0cc05fc541f9ee539dbc073e3d271c5a/CentralServiceLogo.jpg) 

</td>
<td>
<h1>Central Service:</h1>

* https://git.rwth-aachen.de/sascha.welten/padme-central-service

<h1>Station Software:</h1>

* https://git.rwth-aachen.de/sascha.welten/padme-station-software

<h1>Metadata Components:</h1>

* https://git.rwth-aachen.de/sascha.welten/padme-metadata

<h1>Train Depots:</h1>

* https://git.rwth-aachen.de/sascha.welten/padme-train-depot

* https://git.rwth-aachen.de/sascha.welten/padme-federated-train-depot

</td>
</tr>
</table>

# PHT Center Service
## Usage
* Make sure you have install and run PostgreSQL server
* Create database with the name same as in config file
* Run `npm install`
* Run `sequelize db:migrate`
* Run `npm start`

### Options
Following options are supported. They are set via environment variables
<table>
<tr>
<th>ENV Var</th>
<th>Default Value</th>
<th>Description</th>
</tr>
<tr>
<td>POSTGRES_PASSWORD</td>
<td></td>
<td>The password for the Postgres Database</td>
</tr>
<tr>
<td>METADATA_STORE_URL</td>
<td></td>
<td>The enrollment url of the metadata store, e.g. https://metadata.padme-analytics.de/stations/enroll</td>
</tr>
<tr>
<td>STORE_ENROLLMENT_SECRET</td>
<td></td>
<td>The secret used to identify as a central service at the metadata store</td>
</tr>

</table>

### Run as a container

Build an image
 ```
sudo docker build -t pht-center-service .
```
Create a container from the image
```
sudo docker run -d --name pht-center-service -p PORT:3000 --net POSTGRES_NETWORK pht-center-service
 ```



## Packages and Libraries
### Back-End
* [NodeJS](https://nodejs.org/dist/latest-v12.x/docs/api/)
* [Express](https://expressjs.com/)
* [Keycloak-Connect](https://github.com/keycloak/keycloak-nodejs-connect)
* [Lodash](https://lodash.com/docs/)
* [Sequelize](https://sequelize.org/v5/)
### Front-End
* [AngularJS (1.7.9)](https://code.angularjs.org/1.7.9/docs/api)
* [DataTables](https://datatables.net/)
* [Lodash](https://lodash.com/docs/)
* [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) & [UI Bootstrap](http://morgul.github.io/ui-bootstrap4)
### License
Copyright (c) 2021 RWTH University Chair i5 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
