-- This file creates the database schema for managing the global station registry
-- We use the prefix gsr as abbreviation for global station repository

-- This schema can be directly implemented using a MySQL or MariaDB as well as
-- other relational database systems using the same data type names

CREATE TABLE IF NOT EXISTS gsr_global_station (
  id        binary(16),
  version   integer default 1 not null,
  name      varchar(50) not null,
  is_pseudomized boolean default false not null,
  crea_time timestamp default now() not null,
  upd_time  timestamp default now() not null,
  CONSTRAINT gsr_global_station_pk PRIMARY KEY (id)
);
