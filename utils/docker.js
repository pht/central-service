const Docker = require('dockerode');
const path = require('path');
const fs = require('fs');

const dind_client_certs_path = '/usr/src/app/dind-certs-client/certs';

const getInstance = () => {

    const docker = new Docker({
        protocol: 'https',
        host: 'dind',
        port: [Censored],
        ca: fs.readFileSync(path.join(dind_client_certs_path, 'ca.pem')),
        cert: fs.readFileSync(path.join(dind_client_certs_path, 'cert.pem')),
        key: fs.readFileSync(path.join(dind_client_certs_path, 'key.pem'))
    });

    return docker;

}
const docker = getInstance();

const getHarborAuthConfig = () => {

    const auth = {
        username: [Censored],
        password:[Censored],
        email: [Censored],
    };

    // console.log(auth);
    return auth;

}

const pullImageWithAuth = (image) => {

    return new Promise((resolve, reject) => {
        docker.pull(image, { 'authconfig': getHarborAuthConfig() }, (err, stream) => {

            if (err) {
                console.error("Docker pull failed for:" + image + "error:" + err);
                reject(err);
                return;
            }

            docker.modem.followProgress(stream, onFinished);

            function onFinished(err, output) {

                if (err) {
                    console.error("Docker pull failed for:" + image + "error:" + err);
                    reject(err);
                }
                else {
                    resolve(output);
                }
            }
        });
    });
}

const pushImageWithAuth = (image, options) => {

    if (!options)
        options = {}
    options['authconfig'] = getHarborAuthConfig();
    return image.push(options);

}

const getImageLabel = (image) => {

    return new Promise(async (resolve, reject) => {

        try {
            let dockerImage = await docker.getImage(image);
            let inspectResult = await dockerImage.inspect();
            let labels = inspectResult.Config.Labels;
            resolve(labels);
        } catch (error) {
            console.log("getImageLabel error");
            reject(error);
        }

    });

}

//extracts file from the given containers file system as a tar archive
const extractFileAsArchive = (container, filename) =>
{
    return new Promise(async (resolve, reject) => {

        // it throws an exception if file does not exist
        await container.infoArchive(
            {
                'path': filename
            }
        ).catch(err => reject(err));

        // download the file if exists - it returns tar archive
        let getArchiveResult = await container.getArchive(
            {
                'path': filename
            }
        ).catch(err => reject(err));
        resolve(getArchiveResult);
    });
}

//Creates a container from the given image
const createContainer = (image) => 
{
    return new Promise(async (resolve, reject) => {

        //First, pull image
        await pullImageWithAuth(image).catch(err => reject(err)); 
        
        //Create container
        let createTempContainerResult = await docker.createContainer({
            Image: image
        }).catch(err => reject(err));

        resolve(createTempContainerResult);
    });
}


module.exports = {
    pullImageWithAuth,
    pushImageWithAuth,
    getImageLabel,
    createContainer,
    extractFileAsArchive,
    instance : docker,
};