const { getAgentOptions } = require('../vault-certs-client');
const commonUtil = require('./common');

const CONSTANTS = {
    'KV_ENGINE': {
        'RSA':{
            'KEY_NAME':{
                'PUBLIC_KEY':'public_key'
            }
        }
    },
    'TRANSIT_ENGINE': {
        'RSA':{
            'KEY_NAME':{
                'LATEST_VERSION': 'latest_version',
                'PUBLIC_KEY':'public_key',
            }
        }
    },    
}

const options = {
    apiVersion: process.env.VAULT_API_VERSION || 'v1',
    endpoint: `https://${process.env.VAULT_HOST}:${process.env.VAULT_PORT}`,
    token: process.env.VAULT_TOKEN || 'client_token',
    requestOptions : {
        agentOptions: getAgentOptions()
    }
};

const vault = require("node-vault")(options);

getPublicKeyKvEngineName = () => {
    const engineName = 'public_key';
    return engineName;
}

const getStationPublicKeyBasePath = () => {
    // KV-engine name
    const path = getPublicKeyKvEngineName(); 
    return path;
}

// KV version 2
const getStationPublicKeyPath = (username) => {
    const path = commonUtil.combineURLs(getStationPublicKeyBasePath(), `data/${username}`);
    return path;
}

// KV version 2
const getStationPublicKeyListPath = () => {
    const path = commonUtil.combineURLs(getStationPublicKeyBasePath(), 'metadata');
    return path;
}

getTrainTransitEngineName = () => {
    const engineName = 'transit';
    return engineName;
}

const getCentralServicePublicKeyBasePath = () => {
    // transit-engine name
    const transitEngineName = getTrainTransitEngineName();
    const path = commonUtil.combineURLs(transitEngineName, 'keys'); 
    return path;
}

const getCentralServicePublicKeyPath = (jobId) => {
    // LATER we will add job id, each job gets its own key-pair
    if (!jobId)
        jobId = 'central_service_rsa'
    const path = commonUtil.combineURLs(getCentralServicePublicKeyBasePath(), jobId); 
    return path;
}

const getCentralServicePrivateKeyBasePath = () => {
    // transit-engine name
    const transitEngineName = getTrainTransitEngineName();
    const path = commonUtil.combineURLs(transitEngineName, 'export/encryption-key'); 
    return path;
}

const getCentralServicePrivateKeyPath = (jobId) => {
    // LATER we will add job id, each job gets its own key-pair
    if (!jobId)
        jobId = 'central_service_rsa'
    const path = commonUtil.combineURLs(getCentralServicePrivateKeyBasePath(), jobId); 
    return path;
}

const getCentralServiceKeyPairBasePath = () => {
    // transit-engine name
    const transitEngineName = getTrainTransitEngineName();
    const path = commonUtil.combineURLs(transitEngineName, 'keys'); 
    return path;
}

const getCentralServiceKeyPairPath = (jobId) => {
    const path = commonUtil.combineURLs(getCentralServiceKeyPairBasePath(), jobId); 
    return path;
}

module.exports = {
    CONSTANTS: CONSTANTS,
    instance: vault,
    writeStationPublicKey: (username, publicKey) => {
        const keyPath = getStationPublicKeyPath(username);
        const data = {
            public_key : publicKey
        };
        const opts = {
            data
        };

        return vault.write(keyPath, opts);
    },

    readStationPublicKey: (username) => {
        const keyPath = getStationPublicKeyPath(username);
        return vault.read(keyPath);

    },

    listStationPublicKey: () => {
        const listPath = getStationPublicKeyListPath();
        return vault.list(listPath);

    },

    readCentralServicePublicKey: (jobId) => {
        const keyPath = getCentralServicePublicKeyPath(jobId);
        return vault.read(keyPath);
    },

    readCentralServicePrivateKey: (jobId) => {
        const keyPath = getCentralServicePrivateKeyPath(jobId);
        return vault.read(keyPath);
    },
    
    writeCentralServiceKeyPair: (jobId) => {
        const keyPath = getCentralServiceKeyPairPath(jobId);
        // Vault options - https://www.vaultproject.io/api-docs/secret/transit#create-key
        const opts = { "type": "rsa-4096", "name": jobId, "derived": false, "exportable": true, "convergent_encryption": false, "allow_plaintext_backup": true, "deletion_allowed": true };
        return vault.write(keyPath, opts);
    } 

};