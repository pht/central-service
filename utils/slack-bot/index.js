const { App } = require('@slack/bolt');

const SLACK_BOT_TOKEN = [Censored];
const SLACK_SIGNING_SECRET = [Censored];

const getClientInstance = () => {

    try {
        
        const app = new App({
            token: SLACK_BOT_TOKEN,
            signingSecret: SLACK_SIGNING_SECRET,
        });

        const client = app.client;
    
        return Promise.resolve(client);
        
    } catch (error) {
        return Promise.reject(error);
    }
}

const getPadmeChannelId = () => {

    const channelId = process.env.SLACK_PADME_CHANNEL_ID || '[Censored]';
    return channelId;

}

module.exports = {
    getClientInstance,
    getPadmeChannelId
};