module.exports = {

    combineURLs: (baseURL, relativeURL) => {
        return relativeURL
          ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
          : baseURL;
      }
      
}